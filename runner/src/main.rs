use dlopen::symbor::Library;

fn main() {
    let file = std::env::args().skip(1).next().unwrap();
    let lib = Library::open(&file).unwrap();
    let main = unsafe { lib.symbol::<Option<unsafe extern "C" fn() -> i32>>("main") }.unwrap();
    if let Some(main) = *main {
        unsafe {
            main();
        }
    } else {
        eprintln!("{} does not contain a main method", file);
    }
}
