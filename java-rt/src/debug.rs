#[no_mangle]
pub extern "C" fn cscrt_dbg_stack_push(offset: u64, value: u64) {
    println!("push {} {:x}", offset, value);
}

#[no_mangle]
pub extern "C" fn cscrt_dbg_stack_pop(offset: u64, value: u64) {
    println!("pop {} {:x}", offset, value);
}
