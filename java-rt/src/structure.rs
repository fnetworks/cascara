use std::os::raw::c_void;

#[repr(C)]
pub struct JValue {
    pub value: *mut c_void,
    pub typ: u8,
}

#[repr(C)]
pub struct JavaClass {
    pub parent: *const JavaClass,

    pub method_count: u32,
    pub method_table: *const *const c_void,
    pub method_lookup_table: *const u8,

    pub field_count: u32,
    pub field_lookup_table: *const u8,
}

#[repr(C)]
pub struct JavaObject {
    pub class: *const JavaClass,
    pub parent: *const JavaObject,
    pub fields: *mut u64,
}
