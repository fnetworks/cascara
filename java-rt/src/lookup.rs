use std::ffi::CStr;

pub unsafe fn lookup(name: &CStr, ltab: *const u8) -> i32 {
    // index into the lookup table
    let mut l: isize = 0;

    // index into the name
    let mut n: isize = 0;

    // Loop over all of the name's characters
    loop {
        // Get the current name char
        let name_char = *name.as_ptr().offset(n as isize) as u8;
        if name_char == b'\0' {
            break;
        }

        // Loop over the entries in the lookup table
        'ltab_loop: loop {
            // Get the current lookup table's char
            let lookup_char = *ltab.offset(l);

            // Get the action for the current lookup char
            let action = *ltab.offset(l + 1);

            // Position l at the next ltab chunk (which may be a payload)
            l += 2;

            if name_char == lookup_char {
                // Check if the result bit is set
                if action & 0x80 != 0 {
                    // Check if the offset bit is also set
                    if action & 0x40 != 0 {
                        // Check if the current name char is the last one
                        if *name.as_ptr().offset(n as isize + 1) as u8 == b'\0' {
                            // If it is, the result variant of the multi-action entry is used.
                            let mut value = ((action & 0x3F) as u32) << 16;
                            value |= (*ltab.offset(l) as u32) << 8;
                            value |= *ltab.offset(l + 1) as u32;
                            return value as i32;
                        } else {
                            // Otherwise, the offset variant is used.
                            let offset = *ltab.offset(l + 2);

                            // ltab[l + 3] is currently not used.

                            // Position l at the next ltab chunk
                            l += 4;

                            // The offset is specified as 2-byte steps, where 0 is the directly
                            // following chunk.
                            l += offset as isize * 2;

                            break 'ltab_loop;
                        }
                    } else {
                        // Check if the current name char is the last one
                        if *name.as_ptr().offset(n as isize + 1) as u8 == b'\0' {
                            // If it is, we have found the result.
                            let mut value = ((action & 0x3F) as u32) << 16;
                            value |= (*ltab.offset(l) as u32) << 8;
                            value |= *ltab.offset(l + 1) as u32;
                            return value as i32;
                        } else {
                            // Otherwise, the name is not contained in the lookup table
                            return -1;
                        }
                    }
                } else {
                    // The result bit is not set, so the action is used as offset.
                    // It is specified as 2-byte steps, where 0 is the directly following chunk.
                    l += action as isize * 2;
                    break 'ltab_loop;
                }
            } else if lookup_char == b'\0' {
                // The current lookup step did not contain the name char.
                return -1;
            } else {
                // Fetch flags and increment l accordingly
                if action & 0x80 != 0 {
                    l += 2;
                    if action & 0x40 != 0 {
                        l += 2;
                    }
                }
            }
        }

        // move to next name char
        n += 1;
    }

    panic!("The lookup function reached its end; this probably means a broken ltab structure");
}

#[cfg(test)]
mod tests {
    use std::ffi::{CStr, CString};
    const TEST_LTAB: [u8; 54] = [
        b'h', 3, b't', 0x80, 0, 3, b'\0', 0, b'e', 3, b'a', 0x80, 0, 4, b'\0', 0, b'l', 1, b'\0',
        0, b'l', 1, b'\0', 0, b'o', 0xC0, 0, 1, 1, 0, b'\0', 0, b'W', 1, b'\0', 0, b'o', 1, b'\0',
        0, b'r', 1, b'\0', 0, b'l', 1, b'\0', 0, b'd', 0x80, 0, 2, b'\0', 0,
    ];

    #[test]
    fn test_hello() {
        let name = CString::new("hello").unwrap();
        let result = unsafe { super::lookup(&name, TEST_LTAB.as_ptr()) };
        assert_eq!(result, 1);
    }

    #[test]
    fn test_hello_world() {
        let name = CString::new("helloWorld").unwrap();
        let result = unsafe { super::lookup(&name, TEST_LTAB.as_ptr()) };
        assert_eq!(result, 2);
    }

    #[test]
    fn test_t() {
        let name = CString::new("t").unwrap();
        let result = unsafe { super::lookup(&name, TEST_LTAB.as_ptr()) };
        assert_eq!(result, 3);
    }

    #[test]
    fn test_fails() {
        let name = CString::new("not_in_table").unwrap();
        let result = unsafe { super::lookup(&name, TEST_LTAB.as_ptr()) };
        assert_eq!(result, -1);
    }

    #[test]
    fn test_fails_at_different_lengths() {
        let name = CString::new("helloX").unwrap();
        let result = unsafe { super::lookup(&name, TEST_LTAB.as_ptr()) };
        assert_eq!(result, -1);
    }

    #[test]
    fn test_decode_generated() {
        let lbuf = [
            120, 2, 116, 3, 0, 0, 121, 3, 0, 0, 101, 4, 0, 0, 122, 128, 0, 3, 0, 0, 115, 1, 0, 0,
            116, 1, 0, 0, 50, 128, 0, 2, 49, 128, 0, 1, 0, 0,
        ];

        let result = unsafe {
            super::lookup(
                CStr::from_bytes_with_nul_unchecked(b"test1\0"),
                lbuf.as_ptr(),
            )
        };
        assert_eq!(result, 1);

        let result = unsafe {
            super::lookup(
                CStr::from_bytes_with_nul_unchecked(b"test2\0"),
                lbuf.as_ptr(),
            )
        };
        assert_eq!(result, 2);

        let result =
            unsafe { super::lookup(CStr::from_bytes_with_nul_unchecked(b"xyz\0"), lbuf.as_ptr()) };
        assert_eq!(result, 3);

        let result = unsafe {
            super::lookup(
                CStr::from_bytes_with_nul_unchecked(b"notfound\0"),
                lbuf.as_ptr(),
            )
        };
        assert_eq!(result, -1);
    }
}
