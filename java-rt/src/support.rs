use super::{JavaClass, JavaObject};
use std::alloc::{alloc, dealloc, Layout};
use std::ffi::CStr;

use std::os::raw::{c_char, c_void};

#[deny(unsafe_op_in_unsafe_fn)]
#[inline]
unsafe fn lookup<'a, L>(
    name: &CStr,
    object: &'a JavaObject,
    lookup_rt: L,
) -> Option<(u16, &'a JavaObject)>
where
    L: Fn(&JavaClass) -> *const u8,
{
    let mut optional_object: Option<&JavaObject> = Some(object);
    while let Some(object) = optional_object {
        let class = unsafe { object.class.as_ref() }.unwrap();
        let index = unsafe { super::lookup::lookup(name, lookup_rt(class)) };
        if index >= 0 {
            return Some((index as u16, object));
        } else {
            optional_object = unsafe { object.parent.as_ref() };
        }
    }
    None
}

unsafe fn lookup_field_value(name: &CStr, object: &JavaObject) -> Option<u64> {
    lookup(name, object, |class| class.field_lookup_table)
        .map(|(index, object)| *object.fields.offset(index as isize))
}

unsafe fn lookup_method(name: &CStr, object: &JavaObject) -> Option<*const c_void> {
    lookup(name, object, |class| class.method_lookup_table)
        .map(|(index, object)| *(*object.class).method_table.offset(index as isize))
}

#[no_mangle]
pub unsafe extern "C" fn cscrt_get_method(
    name: *const c_char,
    object: *const JavaObject,
) -> *const c_void {
    let name = CStr::from_ptr(name);
    let object = object.as_ref().unwrap();
    lookup_method(name, object).expect(&format!("Method not found: {}", name.to_string_lossy()))
}

#[no_mangle]
pub unsafe extern "C" fn cscrt_get_field(name: *const c_char, object: *const JavaObject) -> u64 {
    let name = CStr::from_ptr(name);
    let object = object.as_ref().unwrap();
    lookup_field_value(name, object).expect(&format!("Field not found: {}", name.to_string_lossy()))
}

#[no_mangle]
pub unsafe extern "C" fn cscrt_set_field(
    name: *const c_char,
    object: *const JavaObject,
    value: u64,
) {
    let name = CStr::from_ptr(name);
    let object = object.as_ref().unwrap();
    let (index, object) = lookup(name, object, |class| class.field_lookup_table)
        .expect(&format!("Field not found: {}", name.to_string_lossy()));
    *object.fields.offset(index as isize) = value;
}

#[no_mangle]
pub unsafe extern "C" fn cscrt_allocate_object(class: *const JavaClass) -> *const JavaObject {
    let fields = alloc(Layout::array::<u64>((*class).field_count as usize).unwrap()) as *mut u64;

    let object = JavaObject {
        class,
        parent: core::ptr::null(),
        fields,
    };

    Box::into_raw(Box::new(object))
}

#[no_mangle]
pub unsafe extern "C" fn cscrt_free_object(object: *mut JavaObject) {
    dealloc(
        (*object).fields as *mut u8,
        Layout::array::<u64>((*(*object).class).field_count as usize).unwrap(),
    );
    let allocated = Box::from_raw(object);
    drop(allocated);
}
