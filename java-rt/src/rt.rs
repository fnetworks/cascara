#![allow(non_snake_case, non_upper_case_globals, unused_variables)]

use std::os::raw::c_void;

use super::structure::{JavaClass, JavaObject};

pub static mut Object_methodTable: [*const c_void; 10] = [
    _ZN4java4lang6Object5cloneEv as *const _,
    _ZN4java4lang6Object6equalsEPN4java4lang6ObjectE as *const _,
    _ZN4java4lang6Object8getClassEv as *const _,
    _ZN4java4lang6Object8hashCodeEv as *const _,
    _ZN4java4lang6Object6notifyEv as *const _,
    _ZN4java4lang6Object9notifyAllEv as *const _,
    _ZN4java4lang6Object8toStringEv as *const _,
    _ZN4java4lang6Object4waitEv as *const _,
    _ZN4java4lang6Object4waitEx as *const _,
    _ZN4java4lang6Object4waitExi as *const _,
];
pub static mut Object_methodLookupTable: [u8; 346] = [
    119, 7, 103, 8, 110, 9, 116, 10, 99, 11, 101, 12, 104, 13, 0, 0, 97, 13, 0, 0, 101, 13, 0, 0,
    111, 13, 0, 0, 111, 13, 0, 0, 108, 13, 0, 0, 113, 13, 0, 0, 97, 13, 0, 0, 105, 13, 0, 0, 116,
    13, 0, 0, 116, 13, 0, 0, 83, 13, 0, 0, 111, 13, 0, 0, 117, 13, 0, 0, 115, 13, 0, 0, 116, 13, 0,
    0, 67, 13, 0, 0, 105, 13, 0, 0, 116, 13, 0, 0, 110, 13, 0, 0, 97, 13, 0, 0, 104, 13, 0, 0, 40,
    13, 0, 0, 108, 15, 0, 0, 102, 15, 0, 0, 114, 15, 0, 0, 101, 15, 0, 0, 108, 15, 0, 0, 67, 15, 0,
    0, 74, 15, 41, 128, 0, 7, 0, 0, 97, 15, 0, 0, 121, 15, 0, 0, 105, 0, 0, 0, 40, 0, 0, 0, 115,
    17, 0, 0, 111, 17, 0, 0, 41, 128, 0, 8, 73, 15, 0, 0, 115, 16, 0, 0, 40, 16, 65, 1, 0, 0, 110,
    1, 0, 0, 41, 128, 0, 0, 0, 0, 40, 15, 0, 0, 100, 15, 0, 0, 41, 128, 0, 9, 0, 0, 115, 12, 0, 0,
    41, 128, 0, 4, 0, 0, 108, 9, 0, 0, 103, 9, 0, 0, 76, 9, 0, 0, 101, 9, 0, 0, 40, 9, 0, 0, 108,
    10, 0, 0, 40, 10, 0, 0, 106, 11, 0, 0, 40, 11, 0, 0, 41, 128, 0, 2, 0, 0, 40, 9, 0, 0, 41, 128,
    0, 6, 0, 0, 97, 7, 0, 0, 41, 128, 0, 3, 0, 0, 41, 128, 0, 5, 0, 0, 118, 1, 0, 0, 97, 1, 0, 0,
    47, 1, 0, 0, 108, 1, 0, 0, 97, 1, 0, 0, 110, 1, 0, 0, 103, 1, 0, 0, 47, 1, 0, 0, 79, 1, 0, 0,
    98, 1, 0, 0, 106, 1, 0, 0, 101, 1, 0, 0, 99, 1, 0, 0, 116, 1, 0, 0, 59, 1, 0, 0, 41, 128, 0, 1,
    0, 0,
];
pub static mut Object_fieldLookupTable: [u8; 2] = [0, 0];

pub static mut Object: JavaClass = JavaClass {
    parent: core::ptr::null(),

    method_count: unsafe { Object_methodTable.len() as u32 },
    method_table: unsafe { Object_methodTable.as_ptr() },
    method_lookup_table: unsafe { Object_methodLookupTable.as_ptr() },

    field_count: 0,
    field_lookup_table: unsafe { Object_fieldLookupTable.as_ptr() },
};

pub static mut PrintStream_methodTable: [*const c_void; 1] =
    [_ZN4java2io11PrintStream7printlnEPN4java4lang6ObjectE as *const _];
pub static mut PrintStream_methodLookupTable: [u8; 110] = [
    112, 1, 0, 0, 114, 1, 0, 0, 105, 1, 0, 0, 110, 1, 0, 0, 116, 1, 0, 0, 108, 1, 0, 0, 110, 1, 0,
    0, 40, 1, 0, 0, 76, 1, 0, 0, 106, 1, 0, 0, 97, 1, 0, 0, 118, 1, 0, 0, 97, 1, 0, 0, 47, 1, 0, 0,
    108, 1, 0, 0, 97, 1, 0, 0, 110, 1, 0, 0, 103, 1, 0, 0, 47, 1, 0, 0, 83, 1, 0, 0, 116, 1, 0, 0,
    114, 1, 0, 0, 105, 1, 0, 0, 110, 1, 0, 0, 103, 1, 0, 0, 59, 1, 0, 0, 41, 128, 0, 0, 0, 0,
];
pub static mut PrintStream_fieldLookupTable: [u8; 2] = [0, 0];

pub static mut PrintStream: JavaClass = JavaClass {
    parent: unsafe { &Object },

    method_count: unsafe { PrintStream_methodTable.len() as u32 },
    method_table: unsafe { PrintStream_methodTable.as_ptr() },
    method_lookup_table: unsafe { PrintStream_methodLookupTable.as_ptr() },

    field_lookup_table: unsafe { PrintStream_fieldLookupTable.as_ptr() },
    field_count: 0,
};

pub static mut PrintStream_Object_inst: JavaObject = JavaObject {
    class: unsafe { &Object },
    parent: core::ptr::null(),
    fields: core::ptr::null_mut(),
};

pub static mut PrintStream_out: JavaObject = JavaObject {
    class: unsafe { &PrintStream },
    parent: unsafe { &PrintStream_Object_inst },
    fields: core::ptr::null_mut(),
};

#[no_mangle]
pub static mut _ZN4java4lang6System3outE: *const JavaObject = unsafe { &PrintStream_out };

#[no_mangle]
pub extern "C" fn _ZN4java2io11PrintStream7printlnEPN4java4lang6ObjectE(
    this: *mut c_void,
    arg: *const c_void,
) {
    println!("{:p} {:p}", this, arg);
}

// class java/lang/Object

// <init>()V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6ObjectC1Ev(this: *mut c_void) {}

// clone()Ljava/lang/Object;
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object5cloneEv(this: *mut c_void) -> *mut c_void {
    core::ptr::null_mut()
}

// equals(Ljava/lang/Object;)Z
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object6equalsEPN4java4lang6ObjectE(
    this: *mut c_void,
    other: *mut c_void,
) -> bool {
    false
}

// getClass()Ljava/lang/Class;
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object8getClassEv(this: *mut c_void) -> *mut c_void {
    core::ptr::null_mut()
}

// hashCode()I
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object8hashCodeEv(this: *mut c_void) -> i32 {
    0
}

// notify()V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object6notifyEv(this: *mut c_void) {}

// notifyAll()V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object9notifyAllEv(this: *mut c_void) {}

// toString()Ljava/lang/String;
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object8toStringEv(this: *mut c_void) -> *mut c_void {
    core::ptr::null_mut()
}

// wait()V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object4waitEv(this: *mut c_void) {}

// wait(J)V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object4waitEx(this: *mut c_void, timeout: i64) {}

// wait(JI)V
#[no_mangle]
pub extern "C" fn _ZN4java4lang6Object4waitExi(this: *mut c_void, timeout: i64, nanos: i32) {}
