use std::fmt::{self, Display, Formatter};

#[derive(Clone, Debug)]
pub struct MethodType {
    pub return_type: JType,
    pub param_types: Vec<JType>,
}

impl MethodType {
    pub fn format_params(&self) -> String {
        let mut result = String::new();
        result.push('(');
        for param in self.param_types.iter() {
            result.push_str(&param.format());
        }
        result.push(')');
        result
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum JType {
    Boolean,
    Byte,
    Char,
    Short,
    Int,
    Long,
    Float,
    Double,
    Void,
    Class(String),
    Array(Box<JType>),
    MethodType(Box<JType>, Vec<JType>),
}

impl JType {
    fn parse_iter<C: Iterator<Item = char>>(src: &mut std::iter::Peekable<C>) -> Self {
        let first = src.next().expect("Empty jtype is invalid");
        match first {
            'Z' => JType::Boolean,
            'B' => JType::Byte,
            'C' => JType::Char,
            'S' => JType::Short,
            'I' => JType::Int,
            'J' => JType::Long,
            'F' => JType::Float,
            'D' => JType::Double,
            'V' => JType::Void,
            'L' => {
                let typ = src.take_while(|p| *p != ';').collect::<String>();
                JType::Class(typ)
            }
            '(' => {
                let mut types = Vec::new();
                while *src.peek().expect("Unexpected end of jtype") != ')' {
                    types.push(Self::parse_iter(src));
                }
                assert_eq!(src.next().unwrap(), ')');
                let return_type = Self::parse_iter(src);
                JType::MethodType(Box::new(return_type), types)
            }
            '[' => {
                let array_type = Self::parse_iter(src);
                JType::Array(Box::new(array_type))
            }
            invalid => panic!("Invalid signature char {}", invalid),
        }
    }

    #[inline]
    pub fn parse(src: &str) -> Self {
        let mut chars = src.chars().peekable();
        let result = Self::parse_iter(&mut chars);
        assert_eq!(chars.next(), None);
        result
    }

    pub fn into_method_type(self) -> MethodType {
        match self {
            Self::MethodType(ret, params) => MethodType {
                return_type: *ret,
                param_types: params,
            },
            _ => panic!(),
        }
    }

    pub fn format(&self) -> String {
        match self {
            JType::Boolean => "Z".to_owned(),
            JType::Byte => "B".to_owned(),
            JType::Char => "C".to_owned(),
            JType::Short => "S".to_owned(),
            JType::Int => "I".to_owned(),
            JType::Long => "J".to_owned(),
            JType::Float => "F".to_owned(),
            JType::Double => "D".to_owned(),
            JType::Void => "V".to_owned(),
            JType::Class(name) => format!("L{};", name),
            JType::Array(t) => format!("[{}", t.format()),
            JType::MethodType(_ret, _p) => todo!(),
        }
    }

    pub fn format_readable(&self) -> String {
        match self {
            JType::Boolean => "boolean".into(),
            JType::Byte => "byte".into(),
            JType::Char => "char".into(),
            JType::Short => "short".into(),
            JType::Int => "int".into(),
            JType::Long => "long".into(),
            JType::Float => "float".into(),
            JType::Double => "double".into(),
            JType::Void => "void".into(),
            JType::Class(name) => name.replace('/', "."),
            JType::Array(t) => format!("{}[]", t.format_readable()),
            JType::MethodType(..) => todo!(),
        }
    }

    fn mangle(&self) -> String {
        match self {
            JType::Byte => "a".to_owned(),
            JType::Short => "s".to_owned(),
            JType::Int => "i".to_owned(),
            JType::Long => "x".to_owned(),
            JType::Boolean => "b".to_owned(),
            JType::Char => "Ds".to_owned(),
            JType::Float => "Df".to_owned(),
            JType::Double => "Dd".to_owned(),
            JType::Array(at) => format!("P{}", at.mangle()),
            JType::Class(name) => format!("P{}", Self::mangle_class(name)),
            _ => panic!(),
        }
    }

    fn mangle_class(class_name: &str) -> String {
        format!(
            "N{}E",
            class_name
                .split("/")
                .map(|p| format!("{}{}", p.len(), p))
                .collect::<String>(),
        )
    }

    fn mangle_class_item_name(class_name: &str, item_name: &str) -> String {
        format!(
            "N{}{}E",
            class_name
                .split("/")
                .map(|p| format!("{}{}", p.len(), p))
                .collect::<String>(),
            if item_name == "<init>" {
                "C1".into()
            } else {
                format!("{}{}", item_name.len(), item_name)
            }
        )
    }

    /// Mangle the method according to the Itanium C++ ABI specification
    pub fn mangle_method(method: &MethodType, class_name: &str, name: &str) -> String {
        format!(
            "_Z{}{}",
            Self::mangle_class_item_name(class_name, name),
            if !method.param_types.is_empty() {
                method
                    .param_types
                    .iter()
                    .map(|p| p.mangle())
                    .collect::<String>()
            } else {
                "v".into()
            }
        )
    }

    /// Mangle the global according to the Itanium C++ ABI specification
    pub fn mangle_global(class_name: &str, name: &str) -> String {
        format!("_Z{}", Self::mangle_class_item_name(class_name, name))
    }
}

impl Display for JType {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            JType::MethodType(ret, params) => write!(
                f,
                "{}({})",
                ret,
                params
                    .iter()
                    .map(|p| format!("{}", p))
                    .collect::<Vec<_>>()
                    .join(", ")
            ),
            JType::Array(typ) => write!(f, "{}[]", typ),
            JType::Class(cls) => write!(f, "{}", cls.replace('/', ".")),
            JType::Void => f.write_str("void"),
            JType::Double => f.write_str("double"),
            JType::Float => f.write_str("float"),
            JType::Long => f.write_str("long"),
            JType::Int => f.write_str("int"),
            JType::Short => f.write_str("short"),
            JType::Char => f.write_str("char"),
            JType::Byte => f.write_str("byte"),
            JType::Boolean => f.write_str("boolean"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::JType;
    #[test]
    fn parse_simple() {
        assert_eq!(JType::parse("Z"), JType::Boolean);
        assert_eq!(JType::parse("B"), JType::Byte);
        assert_eq!(JType::parse("C"), JType::Char);
        assert_eq!(JType::parse("S"), JType::Short);
        assert_eq!(JType::parse("I"), JType::Int);
        assert_eq!(JType::parse("J"), JType::Long);
        assert_eq!(JType::parse("F"), JType::Float);
        assert_eq!(JType::parse("D"), JType::Double);
    }

    #[test]
    fn parse_class() {
        assert_eq!(
            JType::parse("Ljava/lang/String;"),
            JType::Class("java/lang/String".to_owned())
        );
    }

    #[test]
    fn parse_array() {
        assert_eq!(JType::parse("[I"), JType::Array(Box::new(JType::Int)))
    }

    #[test]
    fn parse_method_simple() {
        assert_eq!(
            JType::parse("(I)V"),
            JType::MethodType(Box::new(JType::Void), vec![JType::Int])
        );
    }

    #[test]
    fn parse_method_multi() {
        assert_eq!(
            JType::parse("([Ljava/lang/String;I)Ljava/lang/Object;"),
            JType::MethodType(
                Box::new(JType::Class("java/lang/Object".to_owned())),
                vec![
                    JType::Array(Box::new(JType::Class("java/lang/String".to_owned()))),
                    JType::Int
                ]
            )
        );
    }

    #[test]
    fn mangle_global() {
        assert_eq!(
            JType::mangle_global("java/lang/System", "out"),
            "_ZN4java4lang6System3outE".to_owned()
        )
    }
}
