//! `javadis` is a java class file viewer/disassembler similar to `javap`.
//!
//! For usage information, run `javadis --help`.

use java_bytecode::{ClassAccessFlags, ClassFile, Instruction, JType, MethodAccessFlags};
use std::fs::File;
use std::io::Cursor;
use std::path::{Path, PathBuf};

fn print_usage() {
    eprintln!("Usage: javadis <options> <class files>");
    eprintln!(" Where possible options include:");
    let args = [
        ("--help -h", "Print this usage message"),
        ("--version", "Version information"),
        //("-v --verbose", "Print additional information"),
        //("-l", "Print line number and local variable tables"),
        ("--public", "Show only public classes and members"),
        ("--protected", "Show protected/public classes and members"),
        (
            "--package",
            "Show package/protected/public classes and members (default)",
        ),
        ("-p --private", "Show all classes and members"),
        ("-c", "Disassemble the code"),
        //("-s", "Print internal type signatures"),
        //("--constants", "Show final constants"),
    ];
    for (arg, description) in &args {
        eprintln!("   {:19} {}", arg, description);
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq)]
enum VisibilityLevel {
    Private,
    Package,
    Protected,
    Public,
}

struct Args {
    class_files: Vec<PathBuf>,
    verbose: bool,
    line_tables: bool,
    visibility_level: VisibilityLevel,
    disassemble: bool,
    internal_signatures: bool,
    final_constants: bool,
}

impl Args {
    fn parse() -> Self {
        let args = std::env::args().skip(1);

        let mut result = Self {
            class_files: Vec::new(),
            verbose: false,
            line_tables: false,
            visibility_level: VisibilityLevel::Package,
            disassemble: false,
            internal_signatures: false,
            final_constants: false,
        };

        for arg in args {
            let arg = if arg.starts_with("--") {
                &arg[2..]
            } else if arg.starts_with("-") {
                &arg[1..]
            } else {
                result.class_files.push(PathBuf::from(arg));
                continue;
            };

            match arg {
                "help" | "h" => {
                    print_usage();
                    std::process::exit(0);
                }
                "version" => {
                    eprintln!("{}", env!("CARGO_PKG_VERSION"));
                    std::process::exit(0);
                }
                "verbose" | "v" => {
                    result.verbose = true;
                }
                "l" => {
                    result.line_tables = true;
                }
                "public" => {
                    result.visibility_level = VisibilityLevel::Public;
                }
                "protected" => {
                    result.visibility_level = VisibilityLevel::Protected;
                }
                "package" => {
                    result.visibility_level = VisibilityLevel::Package;
                }
                "p" | "private" => {
                    result.visibility_level = VisibilityLevel::Private;
                }
                "c" => {
                    result.disassemble = true;
                }
                "s" => {
                    result.internal_signatures = true;
                }
                "constants" => {
                    result.final_constants = true;
                }
                unknown => {
                    eprintln!("Error: Unknown option: {}", unknown);
                    eprintln!("Usage: javadis <options> <class files>");
                }
            }
        }

        if result.class_files.is_empty() {
            eprintln!("Error: No classes specified");
            std::process::exit(1);
        }

        result
    }
}

fn load_class(path: &Path) -> ClassFile {
    let file = File::open(path);
    let mut file = match file {
        Ok(f) => f,
        Err(e) => {
            eprintln!(
                "Failed to open class file {}: {}",
                path.to_string_lossy(),
                e
            );
            std::process::exit(2)
        }
    };

    let class_file = ClassFile::load(&mut file);
    match class_file {
        Ok(cls) => cls,
        Err(e) => {
            eprintln!(
                "Failed to parse class file {}: {}",
                path.to_string_lossy(),
                e
            );
            std::process::exit(2)
        }
    }
}

fn main() {
    let args = Args::parse();

    for file in args.class_files {
        let cls = load_class(&file);

        let class_name = cls.const_pool.get_class_name(cls.this_class);

        if let Some(source_file) = cls.source_file() {
            println!(
                "Compiled from \"{}\"",
                cls.const_pool.get_utf8(source_file.source_file)
            );
        }

        if cls.access_flags.contains(ClassAccessFlags::PUBLIC) {
            print!("public ");
        }

        println!("class {} {{", class_name);

        for method in cls.methods.iter() {
            if method.access_flags.contains(MethodAccessFlags::PUBLIC) {
                // always print public members
            } else if method.access_flags.contains(MethodAccessFlags::PROTECTED) {
                if args.visibility_level > VisibilityLevel::Protected {
                    continue;
                }
            } else if method.access_flags.contains(MethodAccessFlags::PRIVATE) {
                if args.visibility_level > VisibilityLevel::Private {
                    continue;
                }
            } else {
                if args.visibility_level > VisibilityLevel::Package {
                    continue;
                }
            }

            print!("  ");
            if method.access_flags.contains(MethodAccessFlags::PUBLIC) {
                print!("public ");
            } else if method.access_flags.contains(MethodAccessFlags::PROTECTED) {
                print!("protected ");
            } else if method.access_flags.contains(MethodAccessFlags::PRIVATE) {
                print!("private ");
            }
            if method.access_flags.contains(MethodAccessFlags::STATIC) {
                print!("static ");
            }

            let descriptor =
                JType::parse(cls.const_pool.get_utf8(method.descriptor_index)).into_method_type();

            let name = cls.const_pool.get_utf8(method.name_index);
            if name == "<init>" {
                print!("{}", class_name);
            } else {
                print!("{} {}", descriptor.return_type, name);
            }

            println!(
                "({});",
                descriptor
                    .param_types
                    .iter()
                    .map(|p| format!("{}", p))
                    .collect::<Vec<_>>()
                    .join(", ")
            );

            if args.disassemble {
                if let Some(code) = method.code() {
                    println!("    Code: ");
                    let mut cursor = Cursor::new(&code.code);
                    while (cursor.position() as usize) < code.code.len() {
                        let pos = cursor.position() as usize;
                        let instr = Instruction::from_stream(&mut cursor, pos).unwrap();
                        print!("    {:4}: {:13} ", pos, instr.name());
                        match instr {
                            Instruction::ALoad(index) => print!("#{}", index),
                            Instruction::ANewArray(p) => print!("#{}", p),
                            Instruction::AStore(p) => print!("#{}", p),
                            Instruction::BIPush(p) => print!("#{}", p),
                            Instruction::CheckCast(p) => print!("#{}", p),
                            Instruction::DLoad(p) => print!("#{}", p),
                            Instruction::DStore(p) => print!("#{}", p),
                            Instruction::FLoad(p) => print!("#{}", p),
                            Instruction::FStore(p) => print!("#{}", p),
                            Instruction::Getfield(p) => print!("#{}", p),
                            Instruction::GetStatic(p) => print!("#{}", p),
                            Instruction::Goto(p) => print!("#{}", p),
                            Instruction::GotoW(p) => print!("#{}", p),
                            Instruction::IfAcmpeq(p) => print!("#{}", p),
                            Instruction::IfAcmpne(p) => print!("#{}", p),
                            Instruction::IfIcmpeq(p) => print!("#{}", p),
                            Instruction::IfIcmpne(p) => print!("#{}", p),
                            Instruction::IfIcmplt(p) => print!("#{}", p),
                            Instruction::IfIcmpge(p) => print!("#{}", p),
                            Instruction::IfIcmpgt(p) => print!("#{}", p),
                            Instruction::IfIcmple(p) => print!("#{}", p),
                            Instruction::Ifeq(p) => print!("#{}", p),
                            Instruction::Ifne(p) => print!("#{}", p),
                            Instruction::Iflt(p) => print!("#{}", p),
                            Instruction::Ifge(p) => print!("#{}", p),
                            Instruction::Ifgt(p) => print!("#{}", p),
                            Instruction::Ifle(p) => print!("#{}", p),
                            Instruction::Ifnonnull(p) => print!("#{}", p),
                            Instruction::Ifnull(p) => print!("#{}", p),
                            Instruction::Iinc(p1, p2) => print!("{}, {}", p1, p2),
                            Instruction::Iload(p) => print!("#{}", p),
                            Instruction::Instanceof(p) => print!("#{}", p),
                            Instruction::InvokeDynamic(p) => print!("#{}", p),
                            Instruction::InvokeInterface(p1, p2) => print!("#{}, {:2}", p1, p2),
                            Instruction::InvokeSpecial(p) => print!("#{}", p),
                            Instruction::InvokeStatic(p) => print!("#{}", p),
                            Instruction::InvokeVirtual(p) => print!("#{}", p),
                            Instruction::Istore(p) => print!("#{}", p),
                            Instruction::Jsr(p) => print!("#{}", p),
                            Instruction::JsrW(p) => print!("#{}", p),
                            Instruction::Ldc(p) => print!("#{}", p),
                            Instruction::LdcW(p) => print!("#{}", p),
                            Instruction::Ldc2W(p) => print!("#{}", p),
                            Instruction::Lload(p) => print!("#{}", p),
                            Instruction::Lookupswitch(default, cases) => {
                                println!("{{ // {}", cases.len());
                                for case in cases.iter() {
                                    println!("{:>24}: {}", case.0, case.1 + pos as i32);
                                }
                                println!("{:>24}: {}", "default", default + pos as i32);
                                print!("          }}");
                            }
                            Instruction::Lstore(p) => print!("#{}", p),
                            Instruction::Multianewarray(p1, p2) => print!("#{}_{}", p1, p2),
                            Instruction::New(p) => print!("#{}", p),
                            Instruction::Newarray(typ) => print!("#{}", typ),
                            Instruction::Putfield(p) => print!("#{}", p),
                            Instruction::Putstatic(p) => print!("#{}", p),
                            Instruction::Ret(p) => print!("#{}", p),
                            Instruction::Sipush(p) => print!("#{}", p),
                            // TODO tableswitch
                            Instruction::Wide(p) => print!("#{}", p),
                            _ => {}
                        }
                        println!();
                    }
                    println!();
                }
            }
        }

        println!("}}");
    }
}
