use std::collections::HashMap;

#[derive(Debug)]
pub struct JavaManifest {
    pub entries: HashMap<String, String>,
}

impl JavaManifest {
    pub fn parse(source: &str) -> Self {
        let mut entries = Vec::<(String, String)>::new();
        for line in source.lines() {
            if line.is_empty() {
                // end section or smth
            } else if line.starts_with(" ") {
                // continuation
                todo!();
            } else {
                let mut split = line.splitn(2, ":");
                let key = split.next().unwrap().trim();
                let value = split.next().unwrap().trim();
                entries.push((key.to_owned(), value.to_owned()));
            }
        }
        let entries = entries.into_iter().collect::<HashMap<_, _>>();
        Self { entries }
    }
}
