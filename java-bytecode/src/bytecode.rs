pub use super::instruction::*;

use byteorder::{BigEndian, ReadBytesExt};
use std::io::{Cursor, Read};

use classfile_parser::attribute_info::AttributeInfo;
use classfile_parser::field_info::FieldInfo;
use classfile_parser::method_info::MethodInfo;

pub use classfile_parser::method_info::MethodAccessFlags;
pub use classfile_parser::ClassAccessFlags;

pub use super::constpool::*;

use super::error::{ClassParseError, ClassParseResult};

#[derive(Debug)]
pub struct ClassFile {
    pub const_pool: ConstantPool,
    pub major_version: u16,
    pub minor_version: u16,
    pub access_flags: ClassAccessFlags,
    pub this_class: ConstPoolIndex,
    pub super_class: ConstPoolIndex,
    pub interfaces: Vec<ConstPoolIndex>,
    pub fields: Vec<FieldInfo>,
    pub methods: Box<[Method]>,
    pub attributes: Vec<Attribute>,
}

impl ClassFile {
    pub fn load<R: Read>(source: &mut R) -> Result<Self, ClassParseError> {
        let mut bytes = Vec::new();
        source.read_to_end(&mut bytes)?;

        let class_file = match classfile_parser::class_parser(&bytes) {
            Ok((_, class_file)) => class_file,
            Err(e) => return Err(ClassParseError::ParseError(e.to_string())),
        };

        let const_pool = ConstantPool::from_vec(class_file.const_pool);

        let attributes: ClassParseResult<Vec<Attribute>> = class_file
            .attributes
            .into_iter()
            .map(|attr| Attribute::from_raw(attr, &const_pool))
            .collect();
        let attributes = attributes?;

        let methods: ClassParseResult<Vec<Method>> = class_file
            .methods
            .into_iter()
            .map(|method| Method::from_raw(method, &const_pool))
            .collect();
        let methods = methods?;

        Ok(Self {
            const_pool,
            major_version: class_file.major_version,
            minor_version: class_file.minor_version,
            access_flags: class_file.access_flags,
            this_class: class_file.this_class.into(),
            super_class: class_file.super_class.into(),
            interfaces: class_file.interfaces.into_iter().map(Into::into).collect(),
            fields: class_file.fields,
            methods: methods.into_boxed_slice(),
            attributes,
        })
    }

    pub fn source_file(&self) -> Option<&SourceFileAttrib> {
        for attrib in self.attributes.iter() {
            if let Attribute::SourceFile(source_file) = attrib {
                return Some(source_file);
            }
        }
        None
    }
}

#[derive(Debug)]
pub struct Method {
    pub access_flags: MethodAccessFlags,
    pub name_index: ConstPoolIndex,
    pub descriptor_index: ConstPoolIndex,
    pub attributes: Box<[Attribute]>,
}

impl Method {
    fn from_raw(m: MethodInfo, const_pool: &ConstantPool) -> ClassParseResult<Self> {
        let attributes = m.attributes;
        let attributes: ClassParseResult<Vec<Attribute>> = attributes
            .into_iter()
            .map(|attr| Attribute::from_raw(attr, const_pool))
            .collect();
        let attributes = attributes?;

        Ok(Self {
            access_flags: m.access_flags,
            name_index: m.name_index.into(),
            descriptor_index: m.descriptor_index.into(),
            attributes: attributes.into_boxed_slice(),
        })
    }

    pub fn code(&self) -> Option<&CodeAttrib> {
        for attrib in self.attributes.iter() {
            if let Attribute::Code(code_attrib) = attrib {
                return Some(code_attrib);
            }
        }
        None
    }
}

#[derive(Debug)]
pub enum Attribute {
    Code(CodeAttrib),
    SourceFile(SourceFileAttrib),
    Unknown { name: String, data: Box<[u8]> },
}

impl Attribute {
    fn from_raw(attr: AttributeInfo, cpool: &ConstantPool) -> ClassParseResult<Self> {
        let name = cpool.get_utf8(attr.attribute_name_index.into());
        Ok(match name {
            "Code" => Attribute::Code(CodeAttrib::read(attr.info, cpool)?),
            "SourceFile" => Attribute::SourceFile(SourceFileAttrib::read(attr.info)?),
            name => {
                //println!("Unknown attribute {}", name);
                Attribute::Unknown {
                    name: name.to_owned(),
                    data: attr.info.into_boxed_slice(),
                }
            }
        })
    }
}

#[derive(Debug)]
pub struct CodeAttrib {
    pub max_stack: u16,
    pub max_locals: u16,
    pub code: Box<[u8]>,
    pub exceptions: Box<[Exception]>,
    pub attributes: Box<[Attribute]>,
}

#[derive(Debug)]
pub struct Exception {
    pub start_pc: u16,
    pub end_pc: u16,
    pub handler_pc: u16,
    pub catch_type: u16,
}

impl CodeAttrib {
    fn read<S: AsRef<[u8]>>(source: S, cpool: &ConstantPool) -> ClassParseResult<Self> {
        let mut src = Cursor::new(source.as_ref());
        let max_stack = src.read_u16::<BigEndian>()?;
        let max_locals = src.read_u16::<BigEndian>()?;

        let code_length = src.read_u32::<BigEndian>()?;
        let mut code = vec![0u8; code_length as usize];
        src.read_exact(&mut code)?;

        let exception_table_length = src.read_u16::<BigEndian>()?;
        let mut exceptions = Vec::with_capacity(exception_table_length as usize);
        for _ in 0..exception_table_length {
            let start_pc = src.read_u16::<BigEndian>()?;
            let end_pc = src.read_u16::<BigEndian>()?;
            let handler_pc = src.read_u16::<BigEndian>()?;
            let catch_type = src.read_u16::<BigEndian>()?;
            exceptions.push(Exception {
                start_pc,
                end_pc,
                handler_pc,
                catch_type,
            });
        }

        let attribute_count = src.read_u16::<BigEndian>()?;
        let mut attributes = Vec::with_capacity(attribute_count as usize);
        for _ in 0..attribute_count {
            let attribute_name_index = src.read_u16::<BigEndian>()?;
            let attribute_length = src.read_u32::<BigEndian>()?;
            let mut info = vec![0u8; attribute_length as usize];
            src.read_exact(&mut info)?;
            let attrib = AttributeInfo {
                attribute_name_index,
                attribute_length,
                info,
            };
            attributes.push(Attribute::from_raw(attrib, cpool)?);
        }

        Ok(Self {
            max_stack,
            max_locals,
            code: code.into_boxed_slice(),
            exceptions: exceptions.into_boxed_slice(),
            attributes: attributes.into_boxed_slice(),
        })
    }
}

#[derive(Debug)]
pub struct SourceFileAttrib {
    pub source_file: ConstPoolIndex,
}

impl SourceFileAttrib {
    fn read<S: AsRef<[u8]>>(source: S) -> ClassParseResult<Self> {
        let mut src = Cursor::new(source.as_ref());
        let source_file = src.read_u16::<BigEndian>()?.into();

        Ok(Self { source_file })
    }
}
