use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io;

#[derive(Debug)]
pub enum ClassParseError {
    IoError(io::Error),
    ParseError(String),
    UnknownOpcode(u8),
    UnknownWideOpcode(u8),
    UnknownArrayType(u8),
}

pub type ClassParseResult<T> = Result<T, ClassParseError>;

impl Display for ClassParseError {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Self::IoError(e) => write!(fmt, "IO error while reading instruction: {}", e),
            Self::ParseError(e) => write!(fmt, "Failed to parse class: {}", e),
            Self::UnknownOpcode(opcode) => write!(fmt, "Unknown opcode {}", opcode),
            Self::UnknownWideOpcode(opcode) => write!(fmt, "Unknown wide opcode {}", opcode),
            Self::UnknownArrayType(opcode) => write!(fmt, "Unknown array type {}", opcode),
        }
    }
}

impl Error for ClassParseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            Self::IoError(e) => Some(e),
            _ => None,
        }
    }
}

impl From<io::Error> for ClassParseError {
    fn from(err: std::io::Error) -> Self {
        Self::IoError(err)
    }
}
