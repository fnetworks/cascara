use classfile_parser::constant_info::ConstantInfo;

use std::fmt::{self, Display, Formatter};

#[derive(Copy, Clone, Debug)]
pub struct ClassNameType {
    pub class: CPI,
    pub name_and_type: CPI,
}

impl ClassNameType {
    pub fn resolve<'a>(&self, pool: &'a ConstantPool) -> (&'a str, (&'a str, &'a str)) {
        (
            pool.get_class_name(self.class),
            pool.get_name_and_type(self.name_and_type).resolve(pool),
        )
    }
}

#[derive(Copy, Clone, Debug)]
pub struct NameAndType {
    pub name: CPI,
    pub descriptor: CPI,
}

impl NameAndType {
    pub fn resolve<'a>(&self, pool: &'a ConstantPool) -> (&'a str, &'a str) {
        (pool.get_utf8(self.name), pool.get_utf8(self.descriptor))
    }
}

#[derive(Debug)]
pub enum Constant {
    Utf8(String),
    Integer(i32),
    Float(f32),
    Long(i64),
    Double(f64),
    Class {
        name: CPI,
    },
    String(CPI),
    FieldRef(ClassNameType),
    MethodRef(ClassNameType),
    InterfaceMethodRef(ClassNameType),
    NameAndType(NameAndType),
    MethodHandle {
        ref_kind: u8,
        reference: CPI,
    },
    MethodType {
        descriptor: CPI,
    },
    InvokeDynamic {
        bootstrap_method_attr: CPI,
        name_and_type: CPI,
    },
    Unusable,
}

#[derive(Debug)]
pub struct ConstantPool {
    constants: Box<[Constant]>,
}

impl ConstantPool {
    pub(crate) fn from_vec(vec: Vec<ConstantInfo>) -> Self {
        let vec = vec
            .into_iter()
            .map(|constant| match constant {
                ConstantInfo::Utf8(utf8) => Constant::Utf8(utf8.utf8_string),
                ConstantInfo::Integer(i) => Constant::Integer(i.value),
                ConstantInfo::Float(f) => Constant::Float(f.value),
                ConstantInfo::Long(l) => Constant::Long(l.value),
                ConstantInfo::Double(d) => Constant::Double(d.value),
                ConstantInfo::Class(class) => Constant::Class {
                    name: class.name_index.into(),
                },
                ConstantInfo::String(string) => Constant::String(string.string_index.into()),
                ConstantInfo::FieldRef(field_ref) => Constant::FieldRef(ClassNameType {
                    class: field_ref.class_index.into(),
                    name_and_type: field_ref.name_and_type_index.into(),
                }),
                ConstantInfo::MethodRef(method_ref) => Constant::MethodRef(ClassNameType {
                    class: method_ref.class_index.into(),
                    name_and_type: method_ref.name_and_type_index.into(),
                }),
                ConstantInfo::InterfaceMethodRef(imethod_ref) => {
                    Constant::InterfaceMethodRef(ClassNameType {
                        class: imethod_ref.class_index.into(),
                        name_and_type: imethod_ref.name_and_type_index.into(),
                    })
                }
                ConstantInfo::NameAndType(nt) => Constant::NameAndType(NameAndType {
                    name: nt.name_index.into(),
                    descriptor: nt.descriptor_index.into(),
                }),
                ConstantInfo::MethodHandle(method_handle) => Constant::MethodHandle {
                    ref_kind: method_handle.reference_kind,
                    reference: method_handle.reference_index.into(),
                },
                ConstantInfo::MethodType(method_type) => Constant::MethodType {
                    descriptor: method_type.descriptor_index.into(),
                },
                ConstantInfo::InvokeDynamic(invdyn) => Constant::InvokeDynamic {
                    bootstrap_method_attr: invdyn.bootstrap_method_attr_index.into(),
                    name_and_type: invdyn.name_and_type_index.into(),
                },
                ConstantInfo::Unusable => Constant::Unusable,
            })
            .collect();
        Self { constants: vec }
    }

    pub fn size(&self) -> usize {
        self.constants.len()
    }

    pub fn get_utf8(&self, index: ConstPoolIndex) -> &str {
        match &self.constants[index.as_index()] {
            Constant::Utf8(utf8_string) => utf8_string.as_ref(),
            u => panic!("Expected Utf8, got {:?}", u),
        }
    }

    pub fn get_class_name(&self, index: ConstPoolIndex) -> &str {
        match &self.constants[index.as_index()] {
            Constant::Class { name } => self.get_utf8(*name),
            u => panic!("Expected Class, got {:?}", u),
        }
    }

    pub fn get_field_ref(&self, index: ConstPoolIndex) -> &ClassNameType {
        match &self.constants[index.as_index()] {
            Constant::FieldRef(cnt) => cnt,
            u => panic!("Expected FieldRef, got {:?}", u),
        }
    }

    pub fn get_method_ref(&self, index: ConstPoolIndex) -> &ClassNameType {
        match &self.constants[index.as_index()] {
            Constant::MethodRef(cnt) => cnt,
            u => panic!("Expected MethodRef, got {:?}", u),
        }
    }

    pub fn get_name_and_type(&self, index: ConstPoolIndex) -> &NameAndType {
        match &self.constants[index.as_index()] {
            Constant::NameAndType(nt) => nt,
            u => panic!("Expected NameAndType, got {:?}", u),
        }
    }

    #[allow(unused)]
    pub fn dump(&self) {
        for (i, ci) in self.constants.iter().enumerate() {
            let stringified = match ci {
                Constant::Utf8(utf8) => format!("{:15} {:?}", "Utf8", utf8),
                Constant::Integer(integer) => format!("{:15} {}", "Integer", integer),
                Constant::Float(float) => format!("{:15} {}", "Float", float),
                Constant::Long(long) => format!("{:15} {}", "Long", long),
                Constant::Double(double) => format!("{:15} {}", "Double", double),
                Constant::Class { name } => format!("{:15} name: @{}", "Class", name),
                Constant::String(string) => format!("{:15} @{}", "String", string),
                Constant::FieldRef(field_ref) => format!(
                    "{:15} class: @{}, name/type: @{}",
                    "FieldRef", field_ref.class, field_ref.name_and_type
                ),
                Constant::MethodRef(method_ref) => format!(
                    "{:15} class: @{}, name/type: @{}",
                    "MethodRef", method_ref.class, method_ref.name_and_type
                ),
                Constant::InterfaceMethodRef(interface_method_ref) => format!(
                    "{:15} class: @{}, name/type: @{}",
                    "InterfaceMethodRef",
                    interface_method_ref.class,
                    interface_method_ref.name_and_type
                ),
                Constant::NameAndType(name_and_type) => format!(
                    "{:15} name: @{}, descriptor: @{}",
                    "NameAndType", name_and_type.name, name_and_type.descriptor
                ),
                Constant::MethodHandle {
                    ref_kind,
                    reference,
                } => format!(
                    "{:15} kind: {}, index: @{}",
                    "MethodHandle", ref_kind, reference
                ),
                Constant::MethodType { descriptor } => {
                    format!("{:15} descriptor: @{}", "MethodType", descriptor)
                }
                Constant::InvokeDynamic {
                    bootstrap_method_attr,
                    name_and_type,
                } => format!(
                    "{:15} bootstrap method attr: @{}, name/type: @{}",
                    "InvokeDynamic", bootstrap_method_attr, name_and_type
                ),
                Constant::Unusable => format!("Unusable"),
            };
            println!("{:2}: {}", i + 1, stringified);
        }
        println!();
    }
}

impl std::ops::Index<ConstPoolIndex> for ConstantPool {
    type Output = Constant;

    fn index(&self, index: ConstPoolIndex) -> &Self::Output {
        &self.constants[index.as_index()]
    }
}

/// Shortcut for `ConstPoolIndex`
pub type CPI = ConstPoolIndex;

/// An index into a `ClassFile`'s `ConstantPool`.
#[derive(Copy, Clone)]
pub struct ConstPoolIndex(u16);

impl ConstPoolIndex {
    fn as_index(self) -> usize {
        if self.0 > 0 {
            self.0 as usize - 1
        } else {
            panic!("Constant pool index == 0")
        }
    }
}

impl From<u8> for ConstPoolIndex {
    fn from(f: u8) -> Self {
        Self(f as u16)
    }
}

impl From<u16> for ConstPoolIndex {
    fn from(f: u16) -> Self {
        Self(f)
    }
}

impl Display for ConstPoolIndex {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Debug for ConstPoolIndex {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "CPI({})", self.0)
    }
}
