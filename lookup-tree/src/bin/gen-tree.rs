use std::io;
use std::io::Write;

fn main() {
    let mut result = Vec::new();

    eprintln!("Enter key/value pairs (Leave key empty to stop):");
    let stdin = io::stdin();
    loop {
        eprint!("Key: ");
        io::stderr().flush().unwrap();
        let mut key = String::new();
        stdin.read_line(&mut key).unwrap();
        let key = key.trim().to_owned();

        if key.is_empty() {
            break;
        }

        eprint!("Value: ");
        io::stderr().flush().unwrap();
        let mut value = String::new();
        stdin.read_line(&mut value).unwrap();
        let value = value.trim();
        let value: u16 = value.parse().unwrap();

        result.push((key, value));
    }

    let tree = lookup_tree::generate_tree(
        &result
            .iter()
            .map(|(k, v)| (k.as_ref(), *v))
            .collect::<Vec<_>>(),
    );
    let tree = lookup_tree::binarify(tree);

    println!("{:?}", tree);
}
