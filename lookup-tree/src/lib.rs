#![feature(map_entry_replace)]

use std::collections::hash_map::{Entry, HashMap};

#[derive(Debug)]
pub enum TreeNode {
    Children(Vec<(char, TreeNode)>),
    Termination(u16),
    Both(Vec<(char, TreeNode)>, u16),
}

enum TempNode<'a> {
    Children(Vec<(&'a str, u16)>),
    Termination(u16),
    Both(Vec<(&'a str, u16)>, u16),
}

pub fn generate_tree(items: &[(&str, u16)]) -> Vec<(char, TreeNode)> {
    let mut step_map = HashMap::<char, TempNode>::new();
    for item in items {
        let chr = item
            .0
            .chars()
            .nth(0)
            .expect("Zero length elements not allowed");
        let rest = &item.0[1..];

        let entry = step_map.entry(chr);

        match entry {
            Entry::Occupied(mut entry) => match entry.get_mut() {
                TempNode::Children(children) => {
                    if !rest.is_empty() {
                        children.push((rest, item.1));
                    } else {
                        let (k, v) = entry.remove_entry();
                        if let TempNode::Children(children) = v {
                            step_map.insert(k, TempNode::Both(children, item.1));
                        } else {
                            unreachable!()
                        }
                    }
                }
                TempNode::Termination(val) => {
                    if !rest.is_empty() {
                        let val = *val;
                        entry.replace_entry(TempNode::Both(vec![(rest, item.1)], val));
                    } else {
                        panic!("Duplicate entry in lookup tree");
                    }
                }
                TempNode::Both(children, _) => {
                    if !rest.is_empty() {
                        children.push((rest, item.1));
                    } else {
                        panic!("Duplicate entry in lookup tree");
                    }
                }
            },
            Entry::Vacant(entry) => {
                if !rest.is_empty() {
                    entry.insert(TempNode::Children(vec![(rest, item.1)]));
                } else {
                    entry.insert(TempNode::Termination(item.1));
                }
            }
        }
    }

    let result = step_map
        .into_iter()
        .map(|(chr, node)| {
            (
                chr,
                match node {
                    TempNode::Termination(value) => TreeNode::Termination(value),
                    TempNode::Children(children) => TreeNode::Children(generate_tree(&children)),
                    TempNode::Both(children, value) => {
                        TreeNode::Both(generate_tree(&children), value)
                    }
                },
            )
        })
        .collect::<Vec<_>>();

    result
}

pub fn binarify<T: AsRef<[(char, TreeNode)]>>(tree_root: T) -> Vec<u8> {
    let mut result = Vec::<u8>::new();

    let mut post_map = HashMap::<usize, usize>::new();

    let mut cur_layer = Vec::<&[(char, TreeNode)]>::new();
    let mut next_layer = Vec::<&[(char, TreeNode)]>::new();

    cur_layer.push(tree_root.as_ref());

    loop {
        for (ii, entry) in cur_layer.drain(..).enumerate() {
            if let Some((key, value)) = post_map.remove_entry(&ii) {
                if ii == key {
                    use std::convert::TryInto;
                    result[value] = ((result.len() - 1 - value) / 2).try_into().unwrap();
                }
            }
            for item in entry {
                result.push(item.0 as u8);
                match &item.1 {
                    TreeNode::Termination(value) => {
                        result.push(0x80);
                        result.push((value >> 8) as u8 & 0xFF);
                        result.push(*value as u8 & 0xFF);
                    }
                    TreeNode::Children(children) => {
                        post_map.insert(next_layer.len(), result.len());
                        next_layer.push(children.as_ref());

                        result.push(0);
                    }
                    TreeNode::Both(children, value) => {
                        result.push(0x80 | 0x40);
                        result.push((value >> 8) as u8 & 0xFF);
                        result.push(*value as u8 & 0xFF);

                        post_map.insert(next_layer.len(), result.len());
                        next_layer.push(children.as_ref());

                        result.push(0);
                        result.push(0);
                    }
                }
            }
            result.push(0);
            result.push(0);
        }
        if next_layer.is_empty() {
            break;
        }
        cur_layer.append(&mut next_layer);
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tree() {
        let items = vec![("test1", 1), ("test2", 2), ("xyz", 3)];
        let result = generate_tree(&items);
        assert_eq!(result.len(), 2);
        // TODO more asserts
    }
}
