use java_bytecode::JType;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum JavaType {
    Byte,
    Short,
    Int,
    Long,
    Float,
    Double,
    Boolean,
    Char,
    Reference,
}

impl JavaType {
    #[allow(unused)]
    pub fn to_int(self) -> u8 {
        match self {
            Self::Byte => 1,
            Self::Short => 2,
            Self::Int => 3,
            Self::Long => 4,
            Self::Float => 5,
            Self::Double => 6,
            Self::Boolean => 7,
            Self::Char => 8,
            Self::Reference => 9,
        }
    }

    pub fn from_jtype(typ: &JType) -> Self {
        match typ {
            JType::Byte => JavaType::Byte,
            JType::Short => JavaType::Short,
            JType::Int => JavaType::Int,
            JType::Long => JavaType::Long,
            JType::Float => JavaType::Float,
            JType::Double => JavaType::Double,
            JType::Boolean => JavaType::Boolean,
            JType::Char => JavaType::Char,
            JType::Class(_) | JType::Array(_) => JavaType::Reference,
            JType::Void | JType::MethodType(..) => panic!(),
        }
    }
}
