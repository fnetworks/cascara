use inkwell::attributes::{Attribute, AttributeLoc};
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::debug_info::{
    DICompileUnit, DWARFEmissionKind, DWARFSourceLanguage, DebugInfoBuilder,
};
use inkwell::module::{FlagBehavior, Module};
use inkwell::types::StructType;
use inkwell::values::FunctionValue;
use inkwell::AddressSpace;

use std::path::Path;

pub struct UnitContext<'ctx> {
    pub context: &'ctx Context,
    pub module: Module<'ctx>,
    pub builder: Builder<'ctx>,
    pub debug_info: DebugInfoBuilder<'ctx>,
    pub compile_unit: DICompileUnit<'ctx>,
}

impl<'ctx> UnitContext<'ctx> {
    /*pub fn java_value_type(&self) -> StructType<'ctx> {
        const NAME: &str = "JavaValue";
        self.module.get_struct_type(NAME).unwrap_or_else(|| {
            let strct = self.context.opaque_struct_type(NAME);
            strct.set_body(
                &[
                    self.context.i64_type().into(),
                    self.context.i8_type().into(),
                ],
                false,
            );
            strct
        })
    } */

    pub fn java_class_type(&self) -> StructType<'ctx> {
        const NAME: &str = "JavaClass";
        self.module.get_struct_type(NAME).unwrap_or_else(|| {
            let strct = self.context.opaque_struct_type(NAME);
            strct.set_body(
                &[
                    /* parent */
                    strct.ptr_type(AddressSpace::Generic).into(),
                    /* method count */
                    self.context.i32_type().into(),
                    /* method table */
                    self.context
                        .i8_type()
                        .ptr_type(AddressSpace::Generic)
                        .ptr_type(AddressSpace::Generic)
                        .into(),
                    /* method lookup table */
                    self.context
                        .i8_type()
                        .ptr_type(AddressSpace::Generic)
                        .into(),
                    /* field_count */
                    self.context.i32_type().into(),
                    /* field lookup table */
                    self.context
                        .i8_type()
                        .ptr_type(AddressSpace::Generic)
                        .into(),
                ],
                false,
            );
            strct
        })
    }

    pub fn java_object_type(&self) -> StructType<'ctx> {
        const NAME: &str = "JavaObject";
        self.module.get_struct_type(NAME).unwrap_or_else(|| {
            let selftype = self.context.opaque_struct_type("JavaObject");
            selftype.set_body(
                &[
                    /* class */
                    self.java_class_type()
                        .ptr_type(AddressSpace::Generic)
                        .into(),
                    /* parent */
                    selftype.ptr_type(AddressSpace::Generic).into(),
                    /* fields */
                    self.context
                        .i64_type()
                        .ptr_type(AddressSpace::Generic)
                        .into(),
                ],
                false,
            );
            selftype
        })
    }

    pub fn java_aot_lookup_function(&self) -> FunctionValue<'ctx> {
        const NAME: &str = "cscrt_get_method";
        self.module.get_function(NAME).unwrap_or_else(|| {
            let func = self.module.add_function(
                NAME,
                self.context
                    .i8_type()
                    .ptr_type(AddressSpace::Generic)
                    .fn_type(
                        &[
                            self.context
                                .i8_type()
                                .ptr_type(AddressSpace::Generic)
                                .into(),
                            self.java_object_type()
                                .ptr_type(AddressSpace::Generic)
                                .into(),
                        ],
                        false,
                    ),
                None,
            );
            func.add_attribute(
                AttributeLoc::Function,
                self.context
                    .create_enum_attribute(Attribute::get_named_enum_kind_id("readonly"), 0),
            );
            func
        })
    }
}

pub struct ContextBuilder<'a> {
    pub module_name: &'a str,
    pub source_file_name: &'a str,
    pub source_directory: &'a Path,
}

impl<'a> ContextBuilder<'a> {
    pub fn build<'b>(self, context: &'b Context) -> UnitContext<'b> {
        let module = context.create_module(self.module_name);
        module.set_source_file_name(self.source_file_name);

        let builder = context.create_builder();

        // Debug info schema version (whatever that is)
        let debug_metadata_version = context.i32_type().const_int(3, false);
        module.add_basic_value_flag(
            "Debug Info Version",
            FlagBehavior::Warning,
            debug_metadata_version,
        );

        // TODO Handle Darwin (only supports DWARF 2)
        let dwarf_version = context.i32_type().const_int(5, false);
        module.add_basic_value_flag("Dwarf Version", FlagBehavior::Warning, dwarf_version);

        let (debug_info, compile_unit) = module.create_debug_info_builder(
            /* allow unresolved */ true,
            /* language */ DWARFSourceLanguage::Java,
            /* filename */ self.source_file_name,
            /* directory */ self.source_directory.to_string_lossy().as_ref(),
            /* producer */ "cascara",
            /* is_optimized */ false,
            /* compiler command line flags */ "",
            /* runtime version */ 0, // TODO ??
            /* split name */ "", // TODO ??
            /* kind */ DWARFEmissionKind::Full,
            /* dwo id */ 0, // TODO ??
            /* split debug inlining */ false, // TODO ??
            /* debug info for profiling */ false,
        );

        UnitContext {
            context,
            module,
            builder,
            debug_info,
            compile_unit,
        }
    }
}
