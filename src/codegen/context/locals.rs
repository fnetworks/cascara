use super::UnitContext;
use crate::codegen::check::JavaType;

use inkwell::types::BasicType;
use inkwell::values::{IntValue, PointerValue};

pub struct LocalsContext<'ctx> {
    pub locals: PointerValue<'ctx>,
    tracking_locals: Vec<Option<JavaType>>,
}

impl<'ctx> LocalsContext<'ctx> {
    pub fn create<T: BasicType<'ctx>>(ctx: &UnitContext<'ctx>, value_type: T, count: u16) -> Self {
        let locals = ctx.builder.build_array_alloca(
            value_type,
            ctx.context.i16_type().const_int(count as u64, false),
            "locals",
        );
        Self {
            locals,
            tracking_locals: vec![None; count as usize],
        }
    }

    pub fn build_load(&self, ctx: &UnitContext<'ctx>, index: u8) -> (IntValue<'ctx>, JavaType) {
        let typ = self
            .tracking_locals
            .get(index as usize)
            .expect(&format!(
                "Local index {} out of bounds ({})",
                index,
                self.tracking_locals.len()
            ))
            .expect(&format!("No local stored at index {}", index));

        let local = unsafe {
            ctx.builder.build_in_bounds_gep(
                self.locals,
                &[ctx.context.i32_type().const_int(index as u64, false)],
                "",
            )
        };
        let local_value = ctx
            .builder
            .build_load(local, "local_value")
            .into_int_value();
        (local_value, typ)
    }

    pub fn build_store(
        &mut self,
        ctx: &UnitContext<'ctx>,
        index: u8,
        value: IntValue<'ctx>,
        typ: JavaType,
    ) {
        self.tracking_locals[index as usize] = Some(typ);
        let local = unsafe {
            ctx.builder.build_in_bounds_gep(
                self.locals,
                &[ctx.context.i32_type().const_int(index as u64, false)],
                "",
            )
        };
        ctx.builder.build_store(local, value);
    }
}
