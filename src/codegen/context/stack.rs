use inkwell::attributes::{Attribute, AttributeLoc};
use inkwell::debug_info::{AsDIScope, DIFlags, DIFlagsConstants, DIScope};
use inkwell::module::Linkage;
use inkwell::types::BasicTypeEnum;
use inkwell::values::{FunctionValue, IntValue, PointerValue};
use inkwell::AddressSpace;

use super::UnitContext;
use crate::codegen::check::JavaType;

pub struct StackContext<'ctx> {
    pub stack: PointerValue<'ctx>,
    pub stack_offset: PointerValue<'ctx>,

    tracking_stack: Vec<JavaType>,

    debug_scope: Option<DIScope<'ctx>>,
}

impl<'ctx> StackContext<'ctx> {
    pub fn create(ctx: &UnitContext<'ctx>, value_type: BasicTypeEnum<'ctx>, size: u16) -> Self {
        let stack = ctx.builder.build_array_alloca(
            value_type,
            ctx.context.i16_type().const_int(size as u64, false),
            "stack",
        );
        let stack_offset = ctx
            .builder
            .build_alloca(ctx.context.i64_type(), "stack_offset");
        ctx.builder
            .build_store(stack_offset, ctx.context.i64_type().const_int(0, false));
        Self {
            stack,
            stack_offset,
            tracking_stack: Vec::new(),
            debug_scope: None,
        }
    }

    pub fn build_push(&mut self, ctx: &UnitContext<'ctx>, value: IntValue, typ: JavaType) {
        self.tracking_stack.push(typ);
        let push_fn = self.get_push_fn(ctx);

        let subprog = ctx
            .builder
            .get_insert_block()
            .unwrap()
            .get_parent()
            .unwrap()
            .get_subprogram()
            .unwrap();
        let loc = ctx.debug_info.create_debug_location(
            &ctx.context,
            0,
            0,
            subprog.as_debug_info_scope(),
            None,
        );
        ctx.builder.set_current_debug_location(ctx.context, loc);

        ctx.builder.build_call(
            push_fn,
            &[self.stack.into(), self.stack_offset.into(), value.into()],
            "",
        );

        ctx.builder.unset_current_debug_location();
    }

    fn get_push_fn(&mut self, ctx: &UnitContext<'ctx>) -> FunctionValue<'ctx> {
        const NAME: &str = "cscrt_stack_push";
        const MANGLED_NAME: &str = "_Z16cscrt_stack_pushPyPyy";
        ctx.module.get_function(MANGLED_NAME).unwrap_or_else(|| {
            let func = ctx.module.add_function(
                MANGLED_NAME,
                ctx.context.void_type().fn_type(
                    &[
                        /* stack */
                        ctx.context
                            .i64_type()
                            .ptr_type(AddressSpace::Generic)
                            .into(),
                        /* stack_offset */
                        ctx.context
                            .i64_type()
                            .ptr_type(AddressSpace::Generic)
                            .into(),
                        /* value */ ctx.context.i64_type().into(),
                    ],
                    false,
                ),
                Some(Linkage::Internal),
            );
            let attributes = [
                (AttributeLoc::Function, "alwaysinline"),
                (AttributeLoc::Function, "nofree"),
                (AttributeLoc::Function, "argmemonly"),
                (AttributeLoc::Param(0), "writeonly"),
            ];
            for attr in &attributes {
                func.add_attribute(
                    attr.0,
                    ctx.context
                        .create_enum_attribute(Attribute::get_named_enum_kind_id(attr.1), 0),
                );
            }

            let p1_type = ctx
                .debug_info
                .create_basic_type("stack", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let p2_type = ctx
                .debug_info
                .create_basic_type("stack_offset", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let p3_type = ctx
                .debug_info
                .create_basic_type("value", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let subroutine_type = ctx.debug_info.create_subroutine_type(
                ctx.compile_unit.get_file(),
                /* return type */ None,
                /* parameter types */
                &[p1_type.as_type(), p2_type.as_type(), p3_type.as_type()],
                DIFlags::PUBLIC,
            );
            let func_scope = ctx.debug_info.create_function(
                /* scope */ self.debug_scope(ctx),
                /* func name */ NAME,
                /* linkage_name */ Some(MANGLED_NAME),
                /* file */ ctx.compile_unit.get_file(),
                /* line_no */ 0,
                /* DIType */ subroutine_type,
                /* is_local_to_unit */ true,
                /* is_definition */ true,
                /* scope_line */ 0,
                /* flags */ DIFlags::PUBLIC,
                /* is_optimized */ false,
            );
            func.set_subprogram(func_scope);

            let prev_bb = ctx.builder.get_insert_block();
            let bb = ctx.context.append_basic_block(func, "entry");
            ctx.builder.position_at_end(bb);

            let prev_di = ctx.builder.get_current_debug_location();
            ctx.builder.unset_current_debug_location();

            let params = func.get_params();
            let stack_ptr = params[0].into_pointer_value();
            let stack_offset_ptr = params[1].into_pointer_value();
            let value = params[2].into_int_value();

            // load the current stack offset
            let stack_offset = ctx
                .builder
                .build_load(stack_offset_ptr, "stack_offset")
                .into_int_value();

            ctx.builder.build_call(
                ctx.module.add_function(
                    "cscrt_dbg_stack_push",
                    ctx.context.void_type().fn_type(
                        &[ctx.context.i64_type().into(), ctx.context.i64_type().into()],
                        false,
                    ),
                    None,
                ),
                &[stack_offset.into(), value.into()],
                "",
            );

            // get a pointer to and write into the current stack item
            let stack_value = unsafe {
                ctx.builder
                    .build_in_bounds_gep(stack_ptr, &[stack_offset], "")
            };
            ctx.builder.build_store(stack_value, value);

            // increment the stack pointer
            let next_stack_offset = ctx.builder.build_int_nuw_add(
                stack_offset,
                ctx.context.i64_type().const_int(1, false),
                "",
            );
            ctx.builder.build_store(stack_offset_ptr, next_stack_offset);

            ctx.builder.build_return(None);

            if let Some(prev_bb) = prev_bb {
                ctx.builder.position_at_end(prev_bb);
            }
            if let Some(prev_di) = prev_di {
                ctx.builder.set_current_debug_location(ctx.context, prev_di);
            }
            func
        })
    }

    pub fn build_pop(&mut self, ctx: &UnitContext<'ctx>) -> (IntValue<'ctx>, JavaType) {
        let typ = self
            .tracking_stack
            .pop()
            .expect("Stack was empty when popping");

        let subprog = ctx
            .builder
            .get_insert_block()
            .unwrap()
            .get_parent()
            .unwrap()
            .get_subprogram()
            .unwrap();
        let loc = ctx.debug_info.create_debug_location(
            &ctx.context,
            0,
            0,
            subprog.as_debug_info_scope(),
            None,
        );
        ctx.builder.set_current_debug_location(ctx.context, loc);

        let value = ctx.builder.build_call(
            self.get_pop_fn(ctx),
            &[self.stack.into(), self.stack_offset.into()],
            "stack_value",
        );
        let value = value.try_as_basic_value().left().unwrap().into_int_value();

        ctx.builder.unset_current_debug_location();

        (value, typ)
    }

    fn get_pop_fn(&mut self, ctx: &UnitContext<'ctx>) -> FunctionValue<'ctx> {
        const NAME: &str = "cscrt_stack_pop";
        const MANGLED_NAME: &str = "_Z15cscrt_stack_popPyPy";
        ctx.module.get_function(MANGLED_NAME).unwrap_or_else(|| {
            let func = ctx.module.add_function(
                MANGLED_NAME,
                ctx.context.i64_type().fn_type(
                    &[
                        /* stack */
                        ctx.context
                            .i64_type()
                            .ptr_type(AddressSpace::Generic)
                            .into(),
                        /* stack_offset */
                        ctx.context
                            .i64_type()
                            .ptr_type(AddressSpace::Generic)
                            .into(),
                    ],
                    false,
                ),
                Some(Linkage::Internal),
            );
            let attributes = [
                (AttributeLoc::Function, "alwaysinline"),
                (AttributeLoc::Function, "nofree"),
                (AttributeLoc::Function, "argmemonly"),
                (AttributeLoc::Param(0), "readonly"),
            ];
            for attr in &attributes {
                func.add_attribute(
                    attr.0,
                    ctx.context
                        .create_enum_attribute(Attribute::get_named_enum_kind_id(attr.1), 0),
                );
            }

            let p1_type = ctx
                .debug_info
                .create_basic_type("stack", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let p2_type = ctx
                .debug_info
                .create_basic_type("stack_offset", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let ret_type = ctx
                .debug_info
                .create_basic_type("value", 64_u64, 0x00, DIFlags::PUBLIC)
                .unwrap();
            let subroutine_type = ctx.debug_info.create_subroutine_type(
                ctx.compile_unit.get_file(),
                /* return type */ Some(ret_type.as_type()),
                /* parameter types */ &[p1_type.as_type(), p2_type.as_type()],
                DIFlags::PUBLIC,
            );
            let func_scope = ctx.debug_info.create_function(
                /* scope */ self.debug_scope(ctx),
                /* func name */ NAME,
                /* linkage_name */ Some(MANGLED_NAME),
                /* file */ ctx.compile_unit.get_file(),
                /* line_no */ 0,
                /* DIType */ subroutine_type,
                /* is_local_to_unit */ true,
                /* is_definition */ true,
                /* scope_line */ 0,
                /* flags */ DIFlags::PUBLIC,
                /* is_optimized */ false,
            );
            func.set_subprogram(func_scope);

            let prev_bb = ctx.builder.get_insert_block();
            let bb = ctx.context.append_basic_block(func, "entry");
            ctx.builder.position_at_end(bb);

            let prev_di = ctx.builder.get_current_debug_location();
            ctx.builder.unset_current_debug_location();

            let params = func.get_params();
            let stack_ptr = params[0].into_pointer_value();
            let stack_offset_ptr = params[1].into_pointer_value();

            // load the current stack offset
            let stack_offset = ctx
                .builder
                .build_load(stack_offset_ptr, "stack_offset")
                .into_int_value();

            // decrement the stack pointer
            let stack_offset = ctx.builder.build_int_nuw_sub(
                stack_offset,
                ctx.context.i64_type().const_int(1, false),
                "",
            );
            ctx.builder.build_store(stack_offset_ptr, stack_offset);

            // get a pointer to and load the current stack item
            let stack_value = unsafe {
                ctx.builder
                    .build_in_bounds_gep(stack_ptr, &[stack_offset], "")
            };
            let stack_value = ctx
                .builder
                .build_load(stack_value, "stack_value")
                .into_int_value();

            ctx.builder.build_call(
                ctx.module.add_function(
                    "cscrt_dbg_stack_pop",
                    ctx.context.void_type().fn_type(
                        &[ctx.context.i64_type().into(), ctx.context.i64_type().into()],
                        false,
                    ),
                    None,
                ),
                &[stack_offset.into(), stack_value.into()],
                "",
            );

            ctx.builder.build_return(Some(&stack_value));

            if let Some(prev_bb) = prev_bb {
                ctx.builder.position_at_end(prev_bb);
            }
            if let Some(prev_di) = prev_di {
                ctx.builder.set_current_debug_location(ctx.context, prev_di);
            }
            func
        })
    }

    fn debug_scope(&mut self, ctx: &UnitContext<'ctx>) -> DIScope<'ctx> {
        if let Some(scope) = self.debug_scope {
            scope
        } else {
            let scope = ctx
                .debug_info
                .create_file("cascara-support.cxx", "/tmp")
                .as_debug_info_scope();
            self.debug_scope = Some(scope);
            scope
        }
    }
}
