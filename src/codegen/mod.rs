use inkwell::context::Context;
use inkwell::debug_info::{AsDIScope, DIFlags, DIFlagsConstants};
use inkwell::module::Linkage;
use inkwell::types::{AnyTypeEnum, BasicType, BasicTypeEnum, FunctionType, StructType};
use inkwell::values::GlobalValue;
use inkwell::AddressSpace;

use java_bytecode::{ClassFile, Constant, Instruction, JType, MethodAccessFlags, MethodType};

use std::convert::TryInto;
use std::io::Cursor;

use lookup_tree;

mod check;
use check::JavaType;

mod instructions;

mod context;
use context::*;

struct ClassData<'a> {
    ns: String,
    class_type: StructType<'a>,
    method_table: GlobalValue<'a>,
    method_lookup_table: GlobalValue<'a>,
}

fn generate_class_data<'a>(ctx: &UnitContext<'a>, class: &ClassFile) -> ClassData<'a> {
    let class_data_ns = format!(
        "{}/{}",
        class.const_pool.get_class_name(class.this_class),
        "private"
    );

    let method_table_type = ctx
        .context
        .i8_type()
        .ptr_type(AddressSpace::Generic)
        .array_type(class.methods.len() as u32);
    let method_table_name = JType::mangle_global(&class_data_ns, "method_table");
    let method_table = ctx
        .module
        .add_global(method_table_type, None, &method_table_name);
    method_table.set_constant(true);

    let mut method_lookup = Vec::new();
    for (i, m) in class.methods.iter().enumerate() {
        method_lookup.push((class.const_pool.get_utf8(m.name_index), i as u16));
    }
    let method_lookup_table_value =
        lookup_tree::binarify(lookup_tree::generate_tree(&method_lookup));
    let method_lookup_table_type = ctx
        .context
        .i8_type()
        .array_type(method_lookup_table_value.len() as u32);
    let method_lookup_table_name = JType::mangle_global(&class_data_ns, "method_lookup_table");
    let method_lookup_table =
        ctx.module
            .add_global(method_lookup_table_type, None, &method_lookup_table_name);
    let method_lookup_table_lvalue = {
        let method_lookup_table_value = method_lookup_table_value
            .iter()
            .map(|x| ctx.context.i8_type().const_int(*x as u64, false))
            .collect::<Vec<_>>();
        ctx.context
            .i8_type()
            .const_array(&method_lookup_table_value)
    };
    method_lookup_table.set_initializer(&method_lookup_table_lvalue);
    method_lookup_table.set_constant(true);

    let class_type = ctx.java_class_type();

    ClassData {
        ns: class_data_ns,
        class_type,
        method_table,
        method_lookup_table,
    }
}

fn try_fn_type<'a>(
    typ: AnyTypeEnum<'a>,
    param_types: &[BasicTypeEnum<'a>],
    is_var_args: bool,
) -> FunctionType<'a> {
    match typ {
        AnyTypeEnum::VoidType(typ) => typ.fn_type(&param_types, is_var_args),
        typ => {
            let typ: BasicTypeEnum = typ.try_into().unwrap();
            typ.fn_type(&param_types, is_var_args)
        }
    }
}

fn method_type_to_llvm<'a>(
    typ: &MethodType,
    context: &UnitContext<'a>,
    this_type: Option<BasicTypeEnum<'a>>,
) -> FunctionType<'a> {
    let mut param_types: Vec<BasicTypeEnum> = typ
        .param_types
        .iter()
        .map(|t| jtype_to_llvm(t, context).try_into().unwrap())
        .collect::<Vec<_>>();
    if let Some(this_type) = this_type {
        param_types.insert(0, this_type);
    }
    try_fn_type(
        jtype_to_llvm(&typ.return_type, context),
        &param_types,
        false,
    )
}

fn jtype_to_llvm<'a>(typ: &JType, context: &UnitContext<'a>) -> AnyTypeEnum<'a> {
    let ctx = context.context;
    match typ {
        JType::Boolean => ctx.bool_type().into(),
        JType::Byte => ctx.i8_type().into(),
        JType::Char => ctx.i16_type().into(),
        JType::Short => ctx.i16_type().into(),
        JType::Int => ctx.i32_type().into(),
        JType::Long => ctx.i64_type().into(),
        JType::Float => ctx.f32_type().into(),
        JType::Double => ctx.f32_type().into(),
        JType::Void => ctx.void_type().into(),
        JType::Class(_) => {
            let class_type = context.java_object_type();
            class_type.ptr_type(AddressSpace::Generic).into()
        }
        JType::Array(inner) => {
            let array_type = jtype_to_llvm(inner, context);
            let array_type: BasicTypeEnum<'a> = array_type
                .try_into()
                .expect("Type can't be made into array");
            array_type.ptr_type(AddressSpace::Generic).into()
        }
        JType::MethodType(ret, params) => {
            let ret = jtype_to_llvm(ret, context);
            let params = params
                .into_iter()
                .map(|p| jtype_to_llvm(p, context).try_into().unwrap())
                .collect::<Vec<BasicTypeEnum<'a>>>();
            try_fn_type(ret, &params, false).into()
        }
    }
}

pub fn generate(class: &ClassFile, source_name: &str, is_main: bool) -> String {
    let pool = &class.const_pool;
    let class_name = pool.get_class_name(class.this_class);

    let context = Context::create();

    let source_file_name = class
        .source_file()
        .map_or(source_name, |sf| pool.get_utf8(sf.source_file));

    let ctx = {
        let builder = ContextBuilder {
            module_name: class_name,
            source_file_name,
            source_directory: std::path::Path::new("."),
        };
        builder.build(&context)
    };

    let class_data = generate_class_data(&ctx, class);

    let this_handle_type = ctx.java_object_type().ptr_type(AddressSpace::Generic);

    let mut functions = Vec::new();

    for func in class.methods.iter() {
        let method_name = pool.get_utf8(func.name_index);
        println!("Processing method {}.{}", class_name, method_name);

        ctx.builder.unset_current_debug_location();

        let linkage = {
            if func
                .access_flags
                .intersects(MethodAccessFlags::PUBLIC | MethodAccessFlags::PROTECTED)
            {
                Some(Linkage::External)
            } else if func.access_flags.intersects(MethodAccessFlags::PRIVATE) {
                Some(Linkage::Internal)
            } else {
                Some(Linkage::External)
            }
        };

        let function_type_string = pool.get_utf8(func.descriptor_index);
        let parsed_type = JType::parse(function_type_string).into_method_type();

        let return_type = jtype_to_llvm(&parsed_type.return_type, &ctx);

        let has_this_value = !func.access_flags.contains(MethodAccessFlags::STATIC);

        let mut params: Vec<BasicTypeEnum> = Vec::new();
        if has_this_value {
            params.push(this_handle_type.into());
        }
        for p in parsed_type.param_types.iter() {
            params.push(jtype_to_llvm(p, &ctx).try_into().unwrap());
        }

        let fn_type = try_fn_type(return_type, &params, false);

        let fn_name = JType::mangle_method(&parsed_type, class_name, method_name);

        let lfn = ctx.module.add_function(&fn_name, fn_type, linkage);

        let di_return_type = if parsed_type.return_type != JType::Void {
            Some(
                ctx.debug_info
                    .create_basic_type(
                        &parsed_type.return_type.format_readable(),
                        0_u64,
                        0x00,
                        DIFlags::PUBLIC,
                    )
                    .unwrap()
                    .as_type(),
            )
        } else {
            None
        };

        let mut di_params = Vec::new();
        for param in parsed_type.param_types.iter() {
            di_params.push(
                ctx.debug_info
                    .create_basic_type(&param.format_readable(), 0_u64, 0x00, DIFlags::PUBLIC)
                    .unwrap()
                    .as_type(),
            );
        }

        let subroutine = ctx.debug_info.create_subroutine_type(
            ctx.compile_unit.get_file(),
            di_return_type,
            &di_params,
            DIFlags::PUBLIC,
        );

        let func_scope = ctx.debug_info.create_function(
            /* scope */ ctx.compile_unit.as_debug_info_scope(),
            /* func name */ &method_name,
            /* linkage name */ Some(&fn_name),
            /* file */ ctx.compile_unit.get_file(),
            /* line no */ 0,
            /* DIType */ subroutine,
            /* is_local_to_unit */ false,
            /* is definition */ true,
            /* scope line */ 0,
            /* flags */ DIFlags::PUBLIC,
            /* is optimized */ false,
        );

        lfn.set_subprogram(func_scope);

        let bb = context.append_basic_block(lfn, "entry");
        ctx.builder.position_at_end(bb);

        if let Some(code) = func.code() {
            let instructions = {
                let mut cursor = Cursor::new(&code.code);
                let mut instructions = Vec::new();
                while (cursor.position() as usize) < code.code.len() {
                    let pos = cursor.position() as usize;
                    let instr = Instruction::from_stream(&mut cursor, pos).unwrap();
                    instructions.push(instr);
                }
                instructions
            };

            let max_stack = code.max_stack;
            let max_locals = code.max_locals;

            let mut locals = if max_locals > 0 {
                Some(LocalsContext::create(
                    &ctx,
                    ctx.context.i64_type(),
                    max_locals,
                ))
            } else {
                None
            };

            let mut stack = if max_stack > 0 {
                Some(StackContext::create(
                    &ctx,
                    ctx.context.i64_type().into(),
                    max_stack,
                ))
            } else {
                None
            };

            if has_this_value {
                let this_param = lfn.get_first_param().unwrap().into_pointer_value();
                let this_param =
                    ctx.builder
                        .build_ptr_to_int(this_param, ctx.context.i64_type(), "");
                locals
                    .as_mut()
                    .unwrap()
                    .build_store(&ctx, 0, this_param, JavaType::Reference);
            }

            for (i, p) in parsed_type.param_types.iter().enumerate() {
                let index = if has_this_value { i + 1 } else { i };
                let typ = JavaType::from_jtype(p);
                let param_value = lfn.get_nth_param(index as u32).unwrap();
                let param_value = match typ {
                    JavaType::Reference => ctx.builder.build_ptr_to_int(
                        param_value.into_pointer_value(),
                        ctx.context.i64_type(),
                        "",
                    ),
                    _ => ctx
                        .builder
                        .build_bitcast(param_value, ctx.context.i64_type(), "")
                        .into_int_value(),
                };
                locals
                    .as_mut()
                    .unwrap()
                    .build_store(&ctx, index as u8, param_value, typ);
            }

            for instruction in instructions {
                gen_instr_block(&ctx, lfn, &format!("{}", instruction));
                match instruction {
                    Instruction::ALoad(_)
                    | Instruction::ALoad0
                    | Instruction::ALoad1
                    | Instruction::ALoad2
                    | Instruction::ALoad3 => {
                        let locals = locals.as_mut().unwrap();
                        let stack = stack.as_mut().unwrap();

                        let local = match instruction {
                            Instruction::ALoad(n) => n,
                            Instruction::ALoad0 => 0,
                            Instruction::ALoad1 => 1,
                            Instruction::ALoad2 => 2,
                            Instruction::ALoad3 => 3,
                            _ => unreachable!(),
                        };

                        instructions::aload(local, &ctx, locals, stack);
                    }
                    Instruction::GetStatic(index) => {
                        let (class_name, (name, typ)) = pool.get_field_ref(index).resolve(&pool);

                        instructions::get_static(
                            class_name,
                            name,
                            typ,
                            &ctx,
                            stack.as_mut().unwrap(),
                        );
                    }
                    Instruction::InvokeStatic(index) => {
                        let (class_name, (name, typ)) = pool.get_method_ref(index).resolve(&pool);
                        instructions::invoke_static(class_name, name, typ, &ctx);
                    }
                    Instruction::Ldc(index) => {
                        let value = &class.const_pool[index];
                        instructions::ldc(
                            value,
                            pool,
                            &class_data.ns,
                            &ctx,
                            stack.as_mut().unwrap(),
                        );
                    }
                    Instruction::InvokeSpecial(index) => {
                        let cnt = match &pool[index] {
                            Constant::MethodRef(cnt) | Constant::InterfaceMethodRef(cnt) => cnt,
                            _ => panic!("CP entry should be (Interface)MethodRef"),
                        };
                        let (class_name, (name, typ)) = cnt.resolve(&pool);
                        instructions::invoke_special(
                            class_name,
                            name,
                            typ,
                            &ctx,
                            stack.as_ref().unwrap(),
                        );
                    }
                    Instruction::InvokeVirtual(index) => {
                        let cnt = pool.get_method_ref(index);
                        let (class_name, (name, typ)) = cnt.resolve(&pool);
                        instructions::invoke_virtual(
                            class_name,
                            name,
                            typ,
                            &ctx,
                            stack.as_mut().unwrap(),
                        );
                    }
                    Instruction::Return => {
                        ctx.builder.build_return(None);
                    }
                    u => unimplemented!("Unhandled instruction {:?}", u),
                }
            }
        }

        functions.push(lfn);
    }

    let functions = functions
        .into_iter()
        .map(|fptr| {
            fptr.as_global_value()
                .as_pointer_value()
                .const_cast(context.i8_type().ptr_type(AddressSpace::Generic))
        })
        .collect::<Vec<_>>();
    let functions = context
        .i8_type()
        .ptr_type(AddressSpace::Generic)
        .const_array(&functions);
    class_data.method_table.set_initializer(&functions);

    if is_main {
        let java_main = ctx
            .module
            .get_function(&JType::mangle_method(
                &java_bytecode::MethodType {
                    return_type: JType::Void,
                    param_types: vec![JType::Array(Box::new(JType::Class(
                        "java/lang/String".into(),
                    )))],
                },
                &class_name,
                "main",
            ))
            .expect("Class declared as main, but no main method found");
        let main_fn =
            ctx.module
                .add_function("main", ctx.context.i32_type().fn_type(&[], false), None);
        let main_bb = ctx.context.append_basic_block(main_fn, "entry");
        ctx.builder.position_at_end(main_bb);
        ctx.builder.build_call(
            java_main,
            &[ctx
                .java_object_type()
                .ptr_type(AddressSpace::Generic)
                .ptr_type(AddressSpace::Generic)
                .const_null()
                .into()],
            "",
        );
        ctx.builder
            .build_return(Some(&context.i32_type().const_zero()));
    }

    ctx.debug_info.finalize();

    match ctx.module.verify() {
        Err(e) => {
            eprintln!("\n================================================================================");
            eprintln!("\x1B[91;1mVerification error:\x1B[0m");
            eprintln!("{}", e.to_string_lossy());
            eprintln!("================================================================================\n");
        }
        Ok(_) => {}
    }

    let result = ctx.module.print_to_string();
    result.to_string()
}

use inkwell::values::FunctionValue;

fn gen_instr_block(ctx: &UnitContext, function: FunctionValue, name: &str) {
    let block = ctx.context.append_basic_block(function, name);
    ctx.builder.build_unconditional_branch(block);
    ctx.builder.position_at_end(block);
}
