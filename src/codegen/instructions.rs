use inkwell::debug_info::AsDIScope;
use inkwell::types::BasicTypeEnum;
use inkwell::values::BasicValueEnum;
use inkwell::AddressSpace;

use super::context::{LocalsContext, StackContext, UnitContext};

use super::check::JavaType;
use java_bytecode::{Constant, ConstantPool, JType};

pub fn aload<'a>(
    local: u8,
    ctx: &UnitContext<'a>,
    locals: &mut LocalsContext<'a>,
    stack: &mut StackContext<'a>,
) {
    let (local_value, local_type) = locals.build_load(ctx, local);
    stack.build_push(ctx, local_value, local_type);
}

pub fn get_static<'a>(
    class_name: &str,
    name: &str,
    typ: &str,
    ctx: &UnitContext<'a>,
    stack: &mut StackContext<'a>,
) {
    let global_name = JType::mangle_global(&class_name, &name);
    let typ = JType::parse(typ);

    let static_type: BasicTypeEnum = match &typ {
        JType::Byte => ctx.context.i8_type().into(),
        JType::Short => ctx.context.i16_type().into(),
        JType::Int => ctx.context.i32_type().into(),
        JType::Long => ctx.context.i64_type().into(),
        JType::Float => ctx.context.f32_type().into(),
        JType::Double => ctx.context.f64_type().into(),
        JType::Char => ctx.context.i16_type().into(),
        JType::Boolean => ctx.context.bool_type().into(),
        JType::Class(_) => ctx
            .java_object_type()
            .ptr_type(AddressSpace::Generic)
            .into(),
        JType::Array(..) => todo!(),
        JType::MethodType(..) | JType::Void => unreachable!(),
    };

    let global = ctx
        .module
        .get_global(&global_name)
        .unwrap_or_else(|| ctx.module.add_global(static_type, None, &global_name));
    let global = global.as_pointer_value();

    let value = ctx.builder.build_load(global, "global_value");

    let value = match value {
        BasicValueEnum::IntValue(i) => i,
        BasicValueEnum::PointerValue(ptr) => {
            ctx.builder
                .build_ptr_to_int(ptr, ctx.context.i64_type(), "")
        }
        _ => unreachable!(),
    };

    let typ = JavaType::from_jtype(&typ);

    stack.build_push(ctx, value, typ);
}

pub fn invoke_static<'a>(class_name: &str, name: &str, typ: &str, ctx: &UnitContext<'a>) {
    let typ = JType::parse(typ).into_method_type();
    let fn_name = JType::mangle_method(&typ, class_name, name);
    let fun = ctx.module.get_function(&fn_name).unwrap_or_else(|| {
        // TODO DLLImport linkage
        ctx.module
            .add_function(&fn_name, ctx.context.void_type().fn_type(&[], false), None)
    });

    let subprog = ctx
        .builder
        .get_insert_block()
        .unwrap()
        .get_parent()
        .unwrap()
        .get_subprogram()
        .unwrap();
    let loc = ctx.debug_info.create_debug_location(
        &ctx.context,
        0,
        0,
        subprog.as_debug_info_scope(),
        None,
    );
    ctx.builder.set_current_debug_location(ctx.context, loc);

    ctx.builder.build_call(fun, &[], "");

    ctx.builder.unset_current_debug_location();
}

pub fn ldc<'a>(
    value: &Constant,
    pool: &ConstantPool,
    ns: &str, // TODO replace ns
    ctx: &UnitContext<'a>,
    stack: &mut StackContext<'a>,
) {
    match value {
        Constant::Integer(i) => {
            let value = ctx.context.i32_type().const_int(*i as u64, true);

            stack.build_push(&ctx, value.into(), JavaType::Int);
        }
        Constant::Float(i) => {
            let value = ctx.context.f32_type().const_float(*i as f64);
            let value = ctx
                .builder
                .build_bitcast(value, ctx.context.i32_type(), "ivalue")
                .into_int_value();

            stack.build_push(&ctx, value, JavaType::Int);
        }
        Constant::String(i) => {
            let char_type = ctx.context.i8_type();

            let global_name = format!("string_{}", i);
            let global = ctx.module.get_global(&global_name).unwrap_or_else(|| {
                let string = pool.get_utf8(*i);
                let string = string
                    .bytes()
                    .map(|s| char_type.const_int(s as u64, false))
                    .chain(std::iter::once(char_type.const_zero()))
                    .collect::<Vec<_>>();
                let global = ctx.module.add_global(
                    char_type.array_type(string.len() as u32),
                    None,
                    &JType::mangle_global(&ns, &format!("string_{}", i)),
                );
                global.set_initializer(&char_type.const_array(&string));
                global.set_constant(true);
                global
            });
            let global = global.as_pointer_value();

            let value = ctx
                .builder
                .build_ptr_to_int(global, ctx.context.i64_type(), "string_p");

            stack.build_push(&ctx, value.into(), JavaType::Reference);
        }
        x => unimplemented!("{:?}", x),
    }
}

#[allow(unused)]
pub fn invoke_special<'a>(
    class_name: &str,
    name: &str,
    typ: &str,
    ctx: &UnitContext<'a>,
    stack: &StackContext<'a>,
) {
    let typ = JType::parse(typ);
    // TODO
    let lookup_fn = ctx.java_aot_lookup_function();
}

pub fn invoke_virtual<'a>(
    _class_name: &str,
    name: &str,
    typ: &str,
    ctx: &UnitContext<'a>,
    stack: &mut StackContext<'a>,
) {
    let typ = JType::parse(typ).into_method_type();

    let lookup_fn = ctx.java_aot_lookup_function();

    let mut params = Vec::<BasicValueEnum>::with_capacity(1 + typ.param_types.len());
    for _ in 0..typ.param_types.len() {
        let (param_val, param_type) = stack.build_pop(&ctx);
        let param_val = match param_type {
            JavaType::Reference => ctx
                .builder
                .build_int_to_ptr(
                    param_val,
                    ctx.java_object_type().ptr_type(AddressSpace::Generic),
                    "",
                )
                .into(),
            _ => param_val.into(),
        };
        params.insert(0, param_val);
        // TODO check param_type
    }

    let (this_val, this_typ) = stack.build_pop(ctx);
    assert_eq!(this_typ, JavaType::Reference);
    let this_val = ctx.builder.build_int_to_ptr(
        this_val,
        ctx.java_object_type().ptr_type(AddressSpace::Generic),
        "",
    );
    params.insert(0, this_val.into());

    let func_type = super::method_type_to_llvm(
        &typ,
        ctx,
        Some(
            ctx.java_object_type()
                .ptr_type(AddressSpace::Generic)
                .into(),
        ),
    );

    let lookup_name = format!("{}{}", name, typ.format_params());

    let namegl = ctx
        .builder
        .build_global_string_ptr(&lookup_name, &format!("lookup_{}", lookup_name));
    let method_ptr = ctx.builder.build_call(
        lookup_fn,
        &[namegl.as_pointer_value().into(), this_val.into()],
        "method_ptr",
    );
    let method_ptr = method_ptr
        .try_as_basic_value()
        .left()
        .unwrap()
        .into_pointer_value();

    let function_ptr_type = func_type.ptr_type(AddressSpace::Generic);
    let fnptr = ctx
        .builder
        .build_bitcast(method_ptr, function_ptr_type, "")
        .into_pointer_value();
    ctx.builder.build_call(fnptr, &params, "");
}
