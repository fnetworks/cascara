#![feature(new_uninit)]
#![feature(maybe_uninit_extra)]

mod codegen;

use zip;

use std::fs;
use std::fs::File;
use zip::ZipArchive;

use std::error::Error;
use std::io::Read;

use java_bytecode::ClassFile;
use java_bytecode::JavaManifest;

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = std::env::args().skip(1);
    let input = args.next().expect("Missing INPUT");
    let output = args.next().expect("Missing OUTPUT");

    let file = File::open(&input).unwrap();

    let mut zip = ZipArchive::new(file).unwrap();

    let mut manifest: Option<JavaManifest> = None;
    let mut classes = Vec::new();

    for i in 0..zip.len() {
        let mut file = zip.by_index(i).unwrap();
        if file.is_dir() {
            continue;
        }

        if file.name().ends_with(".class") {
            classes.push(i);
        } else if file.name().to_lowercase().ends_with("meta-inf/manifest.mf") {
            let mut source = String::with_capacity(file.size() as usize);
            file.read_to_string(&mut source).unwrap();
            manifest = Some(JavaManifest::parse(&source));
        } else {
            println!("Unknown file {} - assuming resource", file.name());
        }
    }

    println!("File information:");
    println!("    Number of classes: {}", classes.len());
    print!("    Has manifest: ");
    if let Some(ref manifest) = manifest {
        println!("Yes ({} entries)", manifest.entries.len());
    } else {
        println!("No");
    }

    let main_class = manifest.as_ref().and_then(|m| m.entries.get("Main-Class"));

    let temp_dir = std::env::temp_dir().join(&format!(
        "cascara_{}",
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    ));
    fs::create_dir_all(&temp_dir).unwrap();

    let mut objs = Vec::new();

    for i in classes {
        let mut file = zip.by_index(i).unwrap();
        let class = ClassFile::load(&mut file).unwrap();
        let is_main_class = if let Some(main_class_name) = main_class {
            let class_name = class
                .const_pool
                .get_class_name(class.this_class)
                .replace('/', ".");
            main_class_name == &class_name
        } else {
            false
        };
        eprintln!("In: {}", file.name());
        let result = codegen::generate(&class, file.name(), is_main_class);

        let ll_out = temp_dir.join("program.ll");
        fs::write(&ll_out, result).unwrap();

        let obj_out = temp_dir.join(&format!("program_{}.o", i));
        let llc_output = std::process::Command::new("llc")
            .args(&[
                ll_out.to_string_lossy().as_ref(),
                "-o",
                obj_out.to_string_lossy().as_ref(),
                "--relocation-model=pic",
                "-O1",
                "--filetype=obj",
            ])
            .output()
            .unwrap();
        if !llc_output.status.success() {
            eprintln!("{}", String::from_utf8(llc_output.stderr).unwrap());
            panic!("Failed to run llc");
        }

        objs.push(obj_out);
    }

    let linker_output = std::process::Command::new("clang")
        .args(&["-o", &output, "-shared", "-fpic", "-fuse-ld=lld"])
        .args(&objs.iter().map(|o| o.to_str().unwrap()).collect::<Vec<_>>())
        .arg("-L/home/fnet/Development/cascara/java-rt/target/debug")
        .args(&["-ljava_rt", "-lrt", "-lpthread", "-ldl"])
        .output()
        .unwrap();
    if !linker_output.status.success() {
        eprintln!("{}", String::from_utf8(linker_output.stderr).unwrap());
        panic!("Failed to run clang");
    }

    Ok(())
}
